import request from '@/config/axios'

export interface SubActivityVO {
  id: number
  activityName: string
  meetTime:number
  priority:number
  beginTime: Date
  locationId: number
  locationName: string
  mainActivityId: number
  mainActivityName: String
  activityTag: string
  dutyer: string
  schedule: boolean
  remark: string
  status: number
  createTime: Date  
}

// 查询用户管理列表
export const getActivityPage = (params: PageParam) => {
  return request.get({ url: '/sub/activity-information/page', params })
}

export const getPersonalActivityPage = (params: PageParam) => {
  return request.get({ url: '/sub/activity-information/personal-page', params })
}
// 查询详情
export const getActivity = (id: number) => {
  return request.get({ url: '/sub/activity-information/get?id=' + id })
}

// 新增
export const createActivity = (data: SubActivityVO) => {
  // 格式化日期
  return request.post({ url: '/sub/activity-information/create', data })
}
//更新
export const updateActivity = (data: SubActivityVO) => {
  return request.put({ url: '/sub/activity-information/update', data })
}

export const updateActivityStatus = (id: number, status: number) => {
  const data = {
    id,
    status
  }
  return request.put({ url: '/sub/activity-information/update-status', data: data })
}

export const deleteActivity = (id: number) => {
  return request.delete({ url: '/sub/activity-information/delete?id=' + id })
}