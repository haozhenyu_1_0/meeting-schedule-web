import request from '@/config/axios'

export interface LocationVO {
  id: number
  resourceName: string
  mainResourceId: number
  status: number
}

// 查询用户管理列表
export const getLocationPage = (params: PageParam) => {
  return request.get({ url: '/location/information/page', params })
}

export const getPersonLocationPage = (params: PageParam) => {
  return request.get({ url: '/location/information/personalPage', params })
}
// 查询用户详情
export const getLocation = (id: number) => {
  return request.get({ url: '/location/information/get?id=' + id })
}

// 新增用户
export const createLocation = (data: LocationVO) => {
  return request.post({ url: '/location/information/create', data })
}

export const updateLocation = (data: LocationVO) => {
  return request.put({ url: '/location/information/update', data })
}

export const updateLocationStatus = (id: number, status: number) => {
  const data = {
    id,
    status
  }
  return request.put({ url: '/location/information/update-status', data: data })
}

export const deleteLocation = (id: number) => {
  return request.delete({ url: '/location/information/delete?id=' + id })
}

export const selectMainActivity = () => {
  return request.get({ url: '/location/information/main-location'})
}
export const selectSubLocation = (id: number) => {
  return request.get({ url: '/location/information/sub-location?id='+id})
}