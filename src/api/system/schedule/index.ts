import request from '@/config/axios'

export interface ScheduleVo {
  id: number
  subActivityId: number
  subActivityName: string
  mainActivityId: number
  mainActivityName: string
  beginTime:Date
  endTime:Date
  locationId: number
  locationName: string
}

// 查询用户管理列表
export const getSchedulePage = (params: PageParam) => {
  return request.get({ url: '/meeting/schedule/page', params })
}

export const getPersonalSchedulePage = (params: PageParam) => {
  return request.get({ url: '/meeting/schedule/personal-page', params })
}