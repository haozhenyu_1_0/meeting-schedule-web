import request from '@/config/axios'
import { da } from 'element-plus/es/locale'

export interface MainActivityVO {
  id: number
  activityName: string
  beginTime: Date
  endTime: Date
  locationId: number
  locationName: string
  activityTag: string
  dutyer: string
  remark: string
  status: number
  createTime: Date  
}

// 查询用户管理列表
export const getActivityPage = (params: PageParam) => {
  return request.get({ url: '/main/activity/page', params })
}

export const getPersonalActivityPage = (params: PageParam) => {
  return request.get({ url: '/main/activity/user-page', params })
}

// 查询详情
export const getActivity = (id: number) => {
  return request.get({ url: '/main/activity/get?id=' + id })
}

// 新增
export const createActivity = (data: MainActivityVO) => {
  // 格式化日期
  data.beginTime = data.beginTime.toISOString();
  data.endTime=data.endTime.toISOString();
  return request.post({ url: '/main/activity/create', data })
}
//更新
export const updateActivity = (data: MainActivityVO) => {
  return request.put({ url: '/main/activity/update', data })
}

export const updateActivityStatus = (id: number, status: number) => {
  const data = {
    id,
    status
  }
  return request.put({ url: '/main/activity/update-status', data: data })
}

export const deleteActivity = (id: number) => {
  return request.delete({ url: '/main/activity/delete?id=' + id })
}

export const getActivityList = () => {
  return request.get({ url: '/main/activity/list'})
}

export const handleSchedule = (mainActivityId:number,beginTime:Date,endTime:Date) => {
  const data = {
    mainActivityId,
    beginTime,
    endTime
  }
  console.log(data);
  return request.post({ url: '/meeting/schedule/startSchedule',data})
}